from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _


class EmailService(models.Model):
    use_tls = models.BooleanField(default=True)
    use_ssl = models.BooleanField(default=False)
    host = models.CharField(max_length=255)
    user = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    port = models.PositiveSmallIntegerField(
        default=587,
        help_text=_('Default 587'),
    )
    is_main = models.BooleanField(default=True)
    sent_from = models.CharField(
        max_length=255,
        help_text=_('Your Name <email@server.com>')
    )
    reply_to = models.EmailField(
        help_text=_('email@server.com'),
    )
    fail_silently = models.BooleanField(default=True)

    use_api = models.BooleanField(default=False)
    api_url = models.CharField(max_length=150, blank=True)
    api_key = models.CharField(max_length=150, blank=True)

    def __str__(self):
        return "{} ({})".format(self.host, self.user)

    def save(self, *args, **kwargs):
        if self.is_main:
            EmailService.objects.exclude(pk=self.pk).update(is_main=False)
        else:
            main_service = EmailService.objects.filter(is_main=True)
            if not main_service:
                self.is_main = True

        super().save(*args, **kwargs)

    def clean(self):
        if self.use_api:
            if not self.api_url:
                raise ValidationError({'api_url': _('API Url required.')})
            if not self.api_key:
                raise ValidationError({'api_key': _('API Key required.')})


class Email(models.Model):
    # Simple status to store sending status
    STATUS_CHOICES = (
        ('sent', _('sent')),
        ('failed', _('failed')),
        ('pending', _('pending'))
    )
    created_at = models.DateTimeField(
        _('created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        _('updated at'),
        auto_now=True
    )
    sent_at = models.DateTimeField(
        _('sent at'),
        blank=True, null=True,
        editable=False
    )
    status = models.CharField(
        max_length=10, editable=False, blank=True, default='pending')
    subject = models.CharField(_('subject'), max_length=150, blank=True)
    body = models.TextField(_('body'), blank=True, null=True)
    recipients = models.ManyToManyField(
        'Lead',
        related_name="%(class)ss",
        blank=True,
    )
    recipients_cc = models.ManyToManyField(
        'Lead',
        related_name="%(class)ss_cc",
        blank=True
    )
    recipients_bcc = models.ManyToManyField(
        'Lead',
        related_name="%(class)ss_bcc",
        blank=True
    )
    sent_via = models.CharField(max_length=20, editable=False)

    class Meta:
        ordering = ['-created_at']

    @property
    def excerpt(self):
        if self.subject:
            return self.subject[:70]
        return ''

    def __str__(self):
        return self.excerpt

    def save(self, *args, **kwargs):
        if not self.subject:
            self.subject = _('(no subject)')
        super().save(*args, **kwargs)


class Lead(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email


class Delivery(models.Model):
    lead = models.ForeignKey(
        Lead,
        on_delete=models.PROTECT,
        editable=False
    )
    email = models.ForeignKey(
        Email,
        on_delete=models.PROTECT,
        editable=False
    )
    response = models.CharField(max_length=20, blank=True, editable=False)
    attempts = models.IntegerField(default=1)
