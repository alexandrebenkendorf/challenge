from django.urls import path
from .utils import send_email
from .views import index

urlpatterns = [
    path('', index, name='index'),
    path('<pk>/send/', send_email, name='send_email'),
]
