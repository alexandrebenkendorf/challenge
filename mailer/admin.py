from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import HttpResponseRedirect

from .models import EmailService, Email, Lead
from .utils import send_email


@admin.register(EmailService)
class EmailServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    change_form_template = 'mailer/admin/change_form.html'
    list_display = [
        'excerpt',
        'status',
        'created_at',
    ]
    autocomplete_fields = ['recipients', 'recipients_cc', 'recipients_bcc']
    readonly_fields = [
        'created_at',
        'updated_at',
        'status',
        'sent_via',
    ]
    actions = ['send']

    def send(self, request, queryset):
        for email in queryset:
            send_email(request, email.pk)
    send.short_description = 'Send email'

    def has_change_permission(self, request, obj=None):
        if obj and obj.status != 'sent':
            return True
        return False

    def response_change(self, request, obj):
        if "_send-email" in request.POST:
            send_email(request, obj.pk)
            return HttpResponseRedirect(request.path_info)
        return super().response_change(request, obj)

    def response_add(self, request, obj, post_url_continue=None):
        if "_send-email" in request.POST:
            obj.save()
            send_email(request, obj.pk)
        return super().response_add(request, obj, post_url_continue=post_url_continue)


class EmailsHistory(admin.TabularInline):
    model = Lead.emails.through
    verbose_name_plural = _('History')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Lead)
class LeadAdmin(admin.ModelAdmin):
    search_fields = ['email']
    inlines = [EmailsHistory]
