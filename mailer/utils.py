import requests
from premailer import transform

from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend
from django.utils.translation import ugettext_lazy as _

from .models import EmailService, Email


def send_email(request, pk):
    """
    Send email through templates (txt and html)
    template = without the extension.
    """
    email_service = EmailService.objects.filter(is_main=True).first()
    if not email_service:
        messages.add_message(
            request, messages.ERROR, 'Please go to Mailer > Email Service and create/update the settings.')
        return False

    try:
        email = Email.objects.get(pk=pk)
    except Email.DoesNotExist:
        messages.add_message(
            request, messages.ERROR, 'Email not found.')
        return False

    if not email.recipients.exists():
        messages.add_message(
            request, messages.ERROR, 'Please add at least one recipient.')
        return False

    error = False

    sender = getattr(email_service, 'sent_from')
    reply_to = getattr(email_service, 'reply_to').split(';')
    recipients = {
        'to': [lead.email for lead in email.recipients.all()],
        'cc': [lead.email for lead in email.recipients_cc.all()],
        'bcc': [lead.email for lead in email.recipients_bcc.all()],
    }

    if getattr(email_service, 'use_api'):
        email.sent_via = 'api'
        api_url = getattr(email_service, 'api_url', None)
        api_key = getattr(email_service, 'api_key', None)
        sent = requests.post(
            api_url,
            auth=("api", api_key),
            data={
                'from': sender,
                'h:Reply-To': reply_to,
                'subject': email.subject,
                'text': email.body,
                **recipients
            })

        if not sent.status_code == 200:
            error = True

    else:
        email.sent_via = 'smtp'
        backend = EmailBackend(
            host=getattr(email_service, 'host'),
            port=getattr(email_service, 'port'),
            username=getattr(email_service, 'user'),
            password=getattr(email_service, 'password'),
            use_tls=getattr(email_service, 'use_tls'),
            use_ssl=getattr(email_service, 'use_ssl'),
            fail_silently=getattr(email_service, 'fail_silently'),
        )
        msg = EmailMultiAlternatives(
            email.subject,
            email.body,
            sender,
            **recipients,
            reply_to=reply_to,
            connection=backend
        )
        msg.content_subtype = 'html'

        # This is critical, otherwise images will be displayed as attachments!
        msg.mixed_subtype = 'related'

        sent = msg.send()

        if not sent == 1:
            error = True

    if error:
        email.status = 'failed'
        msg = 'Failed sending email "{}" !'.format(email.excerpt)
        level = messages.ERROR
    else:
        email.status = 'sent'
        msg = 'Email "{}" sent successfully!'.format(email.excerpt)
        level = messages.SUCCESS

    email.save()
    messages.add_message(request, level, msg)
    print(sent.__dict__)
    return False if error else True
