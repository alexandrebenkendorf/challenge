#Instructions

Create the environment

```
python3 -m venv .venv
```

Run the instalation, migrations and create superuser

```
. .venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
```

The mailer app works without .env settings like EMAIL_HOST etc. All these settings must be set in Admin area in ```Mailer > Email Service```
